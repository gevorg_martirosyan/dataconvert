<?php

namespace FazWaz\Feed\Services;

class CurlService
{
    /**
     * @var string
     */
    private static $username = 'dev';

    /**
     * @var string
     */
    private static $password = 'Thailand13';

    /**
     *  get by url
     *  @param  $url string
     *  @return array
     * */
    public function setUrl($url)
    {
        //Initiate cURL.
        $ch = curl_init($url);


        $username = self::$username;

        $password = self::$password;

        //Create the headers array.
        $headers = [
            'Content-Type: application/json',
            'Authorization: Basic ' . base64_encode("$username:$password")
        ];

        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        // Set the url
        curl_setopt($ch, CURLOPT_URL, $url);

        //Execute the cURL request.
        $response = curl_exec($ch);



        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

        if (curl_errno($ch)) {

            return [
                'status_code' => $httpcode,
                'message' => curl_error($ch),
            ];

        }

        if ($httpcode != 200) {
            return [
                'status_code' => $httpcode,
                'message' => $response,
            ];
        }

        $contentType = curl_getinfo($ch, CURLINFO_CONTENT_TYPE);

        // Closing
        curl_close($ch);

        return [
            'status_code' => 200,
            'data' => $response,
            'content_type' => $contentType,
        ];

    }
}