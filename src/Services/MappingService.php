<?php

namespace FazWaz\Feed\Services;

use Illuminate\Support\Arr;

class MappingService
{
    /**
     * @var $defaultKey
     * */
    protected $defaultKey = null;

    /**
     *  languages
     * @var $lang
     * */
    public $lang = [];

    /**
     *
     * @var $item
     */
    protected $item;

    /**
     * @var $propertyMethodLookup
     */
    protected $propertyMethodLookup = [
        'CurrentPrice' => 'current_price',
        'YearlyRentalPrice' => 'yearly_rental_price',
        'RegionName' => 'region_name',
        'UnitCode' => 'unit_code',
        'UnitId' => 'unit_id',
        'City' => 'city',
        'CountryName' => 'country_name',
        'LocationName' => 'location_name',
        'Street' => 'street',
        'Zip' => 'zip',
        'Latitude' => 'latitude',
        'Longitude' => 'longitude',
        'ProjectName' => 'project_name',
        'PropertyTypeTitle' => 'property_type_title',
        'Bedrooms' => 'bedrooms',
        'Bathrooms' => 'bathrooms',
        'IndoorArea' => 'indoor_area',
        'PlotSize' => 'plot_size',
        'Titles' => 'titles',
        'Descriptions' => 'descriptions',
        'Images' => 'images',
        'UrlSale' => 'url_sale',
        'Feature' => 'feature',
        'Scenery' => 'scenery',
        'IsSale' => 'is_sale',
        'IsRental' => 'is_rental',
        'Floor' => 'floor'
    ];

    public function __construct($item, $lang, $defaultKey)
    {
        $this->item = $item;
        $this->lang = $lang;
        $this->defaultKey = $defaultKey;

    }

    /**
     * Magic getter
     *
     * For a given property, look up using the $propertyMethodLookup array
     *
     * If the lookup cannot be found, return a blank string
     *
     * @param   string  $key
     * @return  string|null
     */
    public function __get($key)
    {
        if (array_key_exists($key, $this->propertyMethodLookup)) {
            return trim( $this->{$this->propertyMethodLookup[$key]}() );
        }
        return '';
    }

    public function isValidString($value)
    {
        if (empty($value) || $value == null) {
            return null;
        }
        return $value;
    }

    public function isValidInteger($value)
    {
        if (empty($value) || $value == null || $value === 0.00) {
            return null;
        }
        return $value;
    }

    public function unit_code()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'unit_code'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function unit_id()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'unit_id'));
        return $arg ?  'U'.$arg : $this->defaultKey;
    }

    public function region_name()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'region.name'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function city()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'city.name'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function country_name()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'country.name'));
        return $arg ?  $arg : $this->defaultKey;
    }


    public function location_name()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'location.name'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function street()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'street'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function zip()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'zip'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function latitude()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'latitude'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function longitude()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'longitude'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function project_name()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'project.name'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function property_type_title()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'propertyType.title'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function bedrooms()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'bedrooms'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function bathrooms()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'bathrooms'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function indoor_area()
    {
        $arg = $this->isValidInteger((float) Arr::get($this->item, 'indoor_area'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function plot_size()
    {
        $arg = $this->isValidInteger((float) Arr::get($this->item, 'plot_size'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function  url_sale()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'url_sale'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function floor()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'floor'));
        return $arg ?  $arg : $this->defaultKey;
    }

    public function current_price()
    {
        $price = (float) Arr::get($this->item, 'current_price',  "0.00");
        if ($price !== 0.00) {
            return $price;
        }
        return $this->defaultKey;
    }

    public function yearly_rental_price()
    {
        $price = (float)preg_replace("/[^0-9\.]/", "", Arr::get($this->item, 'yearly_rental_price', "0.00"));
        if ($price !== 0.00) {
            return $price;
        }
        return $this->defaultKey;
    }

    public function titles()
    {
        $titles = [];

        if (is_object($this->item)) {
           foreach ($this->lang as $lang) {
               $titles[$lang] = $this->item->{'unit_title:'.$lang};
           }
           return json_encode(  $titles );
        }

        $getTitles = Arr::get($this->item, 'name');

        if ($getTitles == null || empty($getTitles)) {
            return $this->defaultKey;
        }

        foreach ($this->lang as $lang) {
            $titles[$lang] = $getTitles;
        }

        return json_encode( $titles );

    }

    public function descriptions()
    {
        $descriptions = [];

        // If description is object
        if (is_object($this->item)) {
            foreach ($this->lang as $lang) {
                $descriptions[$lang] = html_entity_decode('<![CDATA['. str_replace(PHP_EOL, '', $this->item->{'unit_description:'.$lang}) . ']]>');
            }
            return json_encode($descriptions);
        }

        // If description is array
        $getDescriptions = Arr::get($this->item, 'project.description');

        if ($getDescriptions == null || empty($getDescriptions)) {
            return $this->defaultKey;
        }
        foreach ($this->lang as $lang) {
            $descriptions[$lang] = $getDescriptions;
        }

        return json_encode( $descriptions );
    }

    public function images()
    {
        $getImages = Arr::get($this->item, 'album');
        $getImages = Arr::pluck($getImages, 'url');
        if (empty($getImages)) {
            return $this->defaultKey;
        }
        return json_encode($getImages);
    }

    public function feature()
    {
        $data = is_object($this->item)
            ? $this->item->feature->map(function ($feat) {
                return $feat->title;
            })->implode(',') : Arr::get($this->item, 'feature');

        return $data;
    }

    public function scenery()
    {
        $data = is_object($this->item)
            ? $this->item->feature->map(function ($feat) {
                return $feat->title;
            })->implode(',') : Arr::get($this->item, 'scenery');

        return $data;
    }

    public function is_sale()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'is_sale'));

        if ($arg) {
            $arg = preg_replace('/\s+/', '', $arg);

            if (strpos(strtolower($arg), 'yes') || $arg === 1) {
                return 'yes';
            } else {
                return 'no';
            }
        }

        return $this->defaultKey;
    }

    public function is_rental()
    {
        $arg = $this->isValidString(Arr::get($this->item, 'is_rent'));
        if ($arg) {
            $arg = preg_replace('~\x{00a0}~','',$arg);
            if (strpos(strtolower($arg), 'yes') || $arg === 1) {
                return 'yes';
            } else {
                return 'no';
            }
        }

        return $this->defaultKey;
    }


}