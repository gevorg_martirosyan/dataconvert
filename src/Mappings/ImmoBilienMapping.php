<?php

namespace FazWaz\Feed\Mappings;

use Illuminate\Support\Arr;
use FazWaz\Feed\Generate\DataConvert;
use FazWaz\Feed\Services\MappingService;

class ImmoBilienMapping {

    /**
     *  It's  folder name when  upload the generated XML file
     * @var $dirName
     * */
    public $dirName = 'property-listing/immobilienscout24';

    /**
     *  node name for XML file
     * @var $nodeName
     * */
    public $nodeName = 'publishObjects';

    /**
     *  Property field default key if the value is not found
     * @var $defaultKey
     * */
    public $defaultKey = null;

    /**
     *  languages
     * @var $lang
     * */
    public $lang = ['en', 'th'];

    /**
     *
     * @var $forgetKeys array
     * */
    public $forgetKeys = [];

    /**
     *
     * @var $propertyType array
     * */
    public $propertyType = [
        'Townhouse' => 'townhouse',
        'Penthouse' => 'condo',
        'land' => 'land',
        'PrivateIsland' => 'land',
        'Hotel' => 'N/F',
        'Restaurant' => 'N/F',
        'Bar' => 'N/F',
        'Shop' => 'N/F',
        'Office' => 'N/F',
        'Warehouse' => 'N/F',
        'House' => 'house',
        'Villa' => 'villa',
        'Condo' => 'condo',
        'Apartment' => 'apartment',
    ];

    /**
     *  Set Data for convert array and mapping data with that converted  array
     *  @param  $type string
     *  @param  $data array
     *  @return array
     *  @throws
     * */
    public function mappingData($type, $data) {

        $dataConvert = new DataConvert();

        $convertedArray = $dataConvert->makeArray($type, $data);

        if ($convertedArray['status_code'] != 200) {
            return $convertedArray;
        }

        $newData = [];

        foreach ($convertedArray['data'] as $key => $itemData) {
            $mappedPropertyData = $this->item($itemData);

            if ($mappedPropertyData) {
                $newData[$key] = $mappedPropertyData;
            }
        }

        return [
            'status_code' => 200,
            'data' => [
                'realestates' => $newData
            ]
        ];

    }

    /**
     *  Mapping json data to array
     *  @param  $mappingService object
     *  @return boolean
     * */
    public function requireFields($mappingService)
    {
        $requiredFields =  [];
        if (in_array($this->defaultKey, $requiredFields) || in_array('', $requiredFields)) {
            return false;
        }
        return true;
    }

    /**
     *  required fields
     *  @param $itemData
     *  @return array
     * */
    public function item($itemData)
    {

        $mappingService = new MappingService($itemData, $this->lang, $this->defaultKey);

        $requiredFields = $this->requireFields($mappingService);

        if (!$requiredFields) {
            return false;
        }

        $item = [
            'externalId' => $this->getItem($mappingService->UnitId,'externalId'),
            'title' => $this->typeGroup($mappingService->PropertyTypeTitle, 'propertyType'),
            'creationDate' => date('d-m-Y H:i:s'),
            'lastModificationDate' => date('d-m-Y H:i:s'),
            'address' => [
                'street' => $this->unsetKey('address.street'),
                'houseNumber' => $this->getItem($mappingService->RegionName,'address.houseNumber'),
                'postcode' => $this->getItem($mappingService->Zip,'address.postcode'),
                'city' => $this->getItem($mappingService->City,'address.city'),
                'wgs84Coordinate' => [
                    'latitude' => $this->getItem($mappingService->Latitude,'address.wgs84Coordinate.latitude'),
                    'longitude' => $this->getItem($mappingService->Longitude,'address.wgs84Coordinate.longitude'),
                ]
            ],
            'groupNumber' => $this->unsetKey('groupNumber'),
            'descriptionNote' =>  $this->descriptions($mappingService->Descriptions, 'th', 'descriptionNote'),
            'furnishingNote' => $this->unsetKey('furnishingNote'),
            'otherNote' => $this->unsetKey('otherNote'),
            'showAddress' => $this->unsetKey('showAddress'),
            'contact' => $this->unsetKey('contact'),
            'apartmentType' => $this->unsetKey('apartmentType'),
            'floor' => $this->unsetKey('floor'),
            'lift' => $this->unsetKey('lift'),
            'assistedLiving' => $this->unsetKey('assistedLiving'),
            'energyCertificate' => [
                'energyCertificateAvailability' =>$this->unsetKey('energyCertificate.energyCertificateAvailability'),
                'energyCertificateCreationDate' =>$this->unsetKey( 'energyCertificate.energyCertificateCreationDate'),
            ],
            'cellar' => $this->unsetKey('cellar'),
            'handicappedAccessible' => $this->unsetKey('handicappedAccessible'),
            'numberOfParkingSpaces' => $this->unsetKey('numberOfParkingSpaces'),
            'condition' => $this->unsetKey('condition'),
            'lastRefurbishment' => $this->unsetKey('lastRefurbishment'),
            'interiorQuality' => $this->unsetKey('interiorQuality'),
            'constructionYear' => $this->unsetKey('constructionYear'),
            'freeFrom' =>$this->unsetKey('freeFrom'),
            'heatingTypeEnev2014' => $this->unsetKey('heatingTypeEnev2014'),
            'price' =>  $this->prices( $mappingService->CurrentPrice,  'price'),
        ];

        foreach ($this->forgetKeys as $value) {
            Arr::forget($item, $value);
        }

        $this->forgetKeys = [];

        return  $item;
    }

    /**
     *  unset the item keys which not exists
     *  @param $value
     *  @param $key
     *  @return string
     * */
    public  function getItem($value, $key)
    {
        if (!$value) {
            $this->unsetKey($key);
        }

        return $value;
    }

    /**
     *  unset the item keys which not exists
     *  @param $key
     *  @return void
     * */
    public  function unsetKey($key)
    {
        $this->forgetKeys[] = $key;
    }

    /**
     *
     * @param  $typeName string
     * @param  $forgetKey  string
     * @return string
     * */
    public function typeGroup($typeName, $forgetKey)
    {
        $typeName = strtolower($typeName);
        if (!isset($this->propertyType[$typeName])) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        return $this->propertyType[$typeName];
    }

    /**
     *  make descriptions as Dot mapping
     *  @param $descriptions
     *  @param $lang
     *  @param $forgetKey
     *  @return array
     * */
    public function descriptions($descriptions, $lang, $forgetKey)
    {
        $descriptions = json_decode($descriptions);
        if (!isset($descriptions->$lang)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $descriptions->$lang;
    }

    /**
     *  make prices as Dot mapping
     *
     *  @param $getSalePrice
     *  @param $forgetKey
     *
     *  @return array
     * */
    public function prices($getSalePrice , $forgetKey = null)
    {

        if ($getSalePrice > 0.00) {
            return [
                'value' => $getSalePrice,
                'currency' => 'THB'
            ];
        }

        if ($forgetKey) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

    }

    /**
     *  get dd listing types
     * @param  $isSale string
     * @param  $isRental  string
     * @param  $forgetKey  string
     * @return string
     * */
    public function listingTypes($isSale, $isRental, $forgetKey = null)
    {
        if ($isSale && $isRental && $forgetKey) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        if ($isSale == 'yes' ) { return  'apartmentBuy'; };
        if ($isRental == 'yes' ) { return 'apartmentRent'; };
    }
}