<?php

namespace FazWaz\Feed\Mappings;

use Illuminate\Support\Arr;
use FazWaz\Feed\Services\MappingService;
use Exception;
use FazWaz\Feed\Generate\DataConvert;

class DotPropertyMapping {

    /**
     *  It's  folder name when  upload the generated XML file
     * @var $dirName
     * */
    public $dirName = 'property-listing/dotproperty';

    /**
     *  node name for XML file
     * @var $nodeName
     * */
    public $nodeName = 'document';

    /**
     *  Property field default key if the value is not found
     * @var $defaultKey
     * */
    public $defaultKey = null;

    /**
     *  languages
     * @var $lang
     * */
    public $lang = ['en', 'th'];

    /**
     *  property type list for Dot mapping
     * @var $propertyTypes array
     * */
    public $propertyTypes = [
            'Townhouse' => 'townhouse',
            'Penthouse' => 'condo',
            'land' => 'land',
            'PrivateIsland' => 'land',
            'Hotel' => null,
            'Restaurant' => null,
            'Bar' => null,
            'Shop' => null,
            'Office' => null,
            'Warehouse' => null,
            'House' => 'house',
            'Villa' => 'house',
            'Condo' => 'condo',
            'Apartment' => 'condo',
    ];

    /**
     *
     * @var $forgetKeys array
     * */
    public $forgetKeys = [];

    /**
     *  Headers static data
     *  @return array
     * */
    public function header() {
        return [
            'agent_name' => 'FazWaz',
            'language' => 'en',
            'phone' => '+6625088780',
            'email' => 'support@fazwaz.com',
            'website' => 'www.fazwaz.com',
            'updated_at' => date("Y-m-d H:i:s"),
            'country' => 'Thailand',
            'province' => 'Phuket'
        ];
    }

    /**
     *  Set Data for convert array and mapping data with that converted  array
     *  @param  $type string
     *  @param  $data array
     *  @return array
     *  @throws Exception
     * */
    public function mappingData($type, $data) {

        $dataConvert = new DataConvert();

        $convertedArray = $dataConvert->makeArray($type, $data);

        if ($convertedArray['status_code'] != 200) {
            return $convertedArray;
        }

        $newData = [];

        foreach ($convertedArray['data'] as $key => $itemData)
        {
            $mappedPropertyData = $this->item($itemData);
            if ($mappedPropertyData) {
                $newData[$key] = $mappedPropertyData;
            }
        }

        return [
            'status_code' => 200,
            'data' => $this->header() + [
                    'properties' => [
                        'property' =>  $newData
                    ]
                ]
        ];

    }

    /**
     *  Mapping json data to array
     *  @param  $mappingService array
     *  @return boolean
     * */
    public function requireFields($mappingService)
    {
        $requiredFields =  [
            'property_id' => $mappingService->UnitCode,
            'price' => $this->prices($mappingService->CurrentPrice, $mappingService->YearlyRentalPrice),
            'province' =>  $mappingService->RegionName,
            'city' => $mappingService->City,
            'area' => $mappingService->LocationName,
            'type' =>  $this->types($mappingService->PropertyTypeTitle)
        ];

        if (in_array($this->defaultKey, $requiredFields) || in_array('', $requiredFields)) {
            return false;
        }

        return true;
    }

    /**
     *  required fields
     *  @param $itemData
     *  @return array
     * */
    public function item($itemData)
    {
        $mappingService = new MappingService($itemData, $this->lang, $this->defaultKey);

        $requiredFields = $this->requireFields($mappingService);

        if (!$requiredFields) {
            return false;
        }

        $item = [
            'property_id' => $this->getItem($mappingService->UnitCode,'property_id'), // required
            'prices' => $this->prices($mappingService->CurrentPrice, $mappingService->YearlyRentalPrice, 'prices'), // required
            'address' => [
                'province' => $this->getItem($mappingService->RegionName,'address.province'), // required
                'city' => $this->getItem($mappingService->City,'address.city'), // required
                'area' => $this->getItem($mappingService->LocationName,'address.area'), // required
                'street' => $this->getItem($mappingService->Street,'address.street'),
                'zip' =>  $this->getItem($mappingService->Zip,'address.zip'),
                'gps_lat' => $this->getItem($mappingService->Latitude,'address.gps_lat'),
                'gps_lon' => $this->getItem($mappingService->Longitude,'address.gps_lon'),
            ],
            'project' => $this->getItem($mappingService->ProjectName,'project'),
            'details' => [
                'type' => $this->types($mappingService->PropertyTypeTitle), // required
                'sub_type' => $this->unsetKey('details.sub_type'),
                'bedrooms' => $this->getItem($mappingService->Bedrooms,'details.bedrooms'),
                'bathrooms'  => $this->getItem($mappingService->Bathrooms,'details.bathrooms'),
                'floor_size' => [
                    '@attributes' => ['unit' => 'sqm'],
                    '@value' => round($this->getItem($mappingService->PlotSize,'details.floor_size'), 2),
                ],
                'land_size' => [
                    '@attributes' => ['unit' => 'sqm'],
                    '@value' =>  round($this->getItem($mappingService->IndoorArea,'details.land_size'), 2),
                ],
                'titles'  =>  $this->titles($mappingService->Titles, 'details.titles'),
                'descriptions' =>   $this->descriptions($mappingService->Descriptions, 'details.descriptions'),
            ],
            'images' =>  $this->images($mappingService->Images, 'images') ,
            'datasource' => $this->getItem($mappingService->UrlSale,'datasource'),
        ];

         foreach ($this->forgetKeys as $value) {
             Arr::forget($item, $value);
         }

         $this->forgetKeys = [];

         return  $item;
    }

    /**
     *  unset the item keys which not exists
     *  @param $value
     *  @param $key
     *  @return string
     * */
    public  function getItem($value, $key)
    {
        if (!$value) {
            $this->unsetKey($key);
        }

        return $value;
    }

    /**
     *  unset the item keys which not exists
     *  @param $key
     *  @return void
     * */
    public  function unsetKey($key)
    {
        $this->forgetKeys[] = $key;
    }

    /**
     *  search type name from $propertyTypes array
     *  @param $data
     *  @return array
     * */
    public function types($data)
    {
         if (!isset($this->propertyTypes[$data])) {
             return $this->defaultKey;
         }
         return $this->propertyTypes[$data];
    }

    /**
     *  make images as Dot mapping
     *  @param $images
     *  @param $forgetKey
     *  @return array
     * */
    public function images($images, $forgetKey = null)
    {
        $getImages = json_decode($images);

        if (empty($getImages) && $forgetKey) {
            $this->unsetKey($forgetKey);
        }

        foreach ($getImages as $key => $image) {
            $arr = [
                '@attributes' => ['number' => $key + 1],
                '@value' => $image
            ];
            $imagesArray[] = $arr;
        }
        return [
            'image' => $imagesArray
        ];
    }

    /**
     *  make descriptions as Dot mapping
     *  @param $descriptions
     *  @return array
     * */
    public function descriptions($descriptions, $forgetKey = null)
    {
        if (!$descriptions) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        $descriptions = json_decode($descriptions);

        $responseData = [];

        foreach ($this->lang as $lang) {

            if (!isset($descriptions->$lang)) {
                continue;
            }

            $description = $descriptions->$lang;

            $responseData['description'][] = [
                '@attributes' => ['lang' => $lang],
                '@value' => $description,
            ];
        }

        return $responseData;
    }

    /**
     *  make titles as Dot mapping
     *  @param $titles
     *  @return array
     * */
     public function titles($titles, $forgetKey = null)
     {
         $responseData = [];

         if (!$titles) {
             $this->unsetKey($forgetKey);
             return $this->defaultKey;
         }

         $titles = json_decode($titles);

         foreach ($this->lang as $lang) {

             if (!isset($titles->$lang)) {
                 continue;
             }

             $title = $titles->$lang;


             $responseData['title'][] = [
                 '@attributes' => ['lang' => $lang],
                 '@value' => $title,
             ];
         }

         return $responseData;
     }

    /**
     *  make prices as Dot mapping
     *
     *  @param $getSalePrice
     *  @param $getRentalPrice
     *  @param $forgetKey
     *
     *  @return array
     * */
     public function prices($getSalePrice , $getRentalPrice, $forgetKey = null)
     {
         $prices = [];

         if ($getRentalPrice > 0.00 ) {
             $prices['price'][] = [
                 '@attributes' => [
                     'tenure' => 'rent',
                     'currency' => 'THB',
                     'period' => 'monthly'
                 ],
                 '@value' => $getRentalPrice,
             ];
         }

         if ($getSalePrice > 0.00) {
             $prices['price'][] = [
                 '@attributes' => [
                     'tenure' => 'sale',
                     'currency' => 'THB'
                 ],
                 '@value' => $getSalePrice,
             ];
         }

         if (empty($prices) && $forgetKey) {
             $this->unsetKey($forgetKey);
         }

         return $prices;
     }

}