<?php

namespace FazWaz\Feed\Mappings;

use Illuminate\Support\Arr;
use FazWaz\Feed\Generate\DataConvert;
use FazWaz\Feed\Services\MappingService;

class TrovitPropertyMapping {

    /**
     *  It's  folder name when  upload the generated XML file
     * @var $dirName
     * */
    public $dirName = 'property-listing/trovit';

    /**
     *  node name for XML file
     * @var $nodeName
     * */
    public $nodeName = 'trovit';

    /**
     *  Property field default key if the value is not found
     * @var $defaultKey
     * */
    public $defaultKey = null;

    /**
     *  languages
     * @var $lang
     * */
    public $lang = ['en', 'th'];

    /**
     *
     * @var $forgetKeys array
     * */
    public $forgetKeys = [];

    /**
     *
     * @var $propertyType array
     * */
    public $propertyType = [
        'Townhouse' => 'house',
        'Penthouse' => 'condo',
        'land' => 'land',
        'PrivateIsland' => 'land',
        'Hotel' => null,
        'Restaurant' => null,
        'Bar' => null,
        'Shop' => null,
        'Office' => null,
        'Warehouse' => null,
        'House' => 'house',
        'Villa' => 'villa',
        'Condo' => 'condo',
        'Apartment' => 'apartment'
    ];

    /**
     *  Set Data for convert array and mapping data with that converted  array
     *  @param  $type string
     *  @param  $data array
     *  @return array
     *  @throws \Exception
     * */
    public function mappingData($type, $data) {

        $dataConvert = new DataConvert();

        $convertedArray = $dataConvert->makeArray($type, $data);

        if ($convertedArray['status_code'] != 200) {
            return $convertedArray;
        }

        $newData = [];

        foreach ($convertedArray['data'] as $key => $itemData) {
            $mappedPropertyData = $this->item($itemData);

            if ($mappedPropertyData) {
                $newData[$key] = $mappedPropertyData;
            }
        }

        return [
            'status_code' => 200,
             'data' => [
                 'ad' => $newData
             ]
        ];

    }


    /**
     *  Mapping json data to array
     *  @param  $mappingService object
     *  @return boolean
     * */
    public function requireFields($mappingService)
    {

        $requiredFields =  [
            'id' => $mappingService->UnitId,
            'url' =>  $mappingService->UrlSale,
//            'title' =>  $this->title($mappingService->Titles, 'th', 'title'),
//            'type' =>  $this->listingTypes($mappingService->IsSale,  $mappingService->IsRental,  'type'),
//            'content' =>  $this->descriptions($mappingService->Descriptions, 'th', 'content'),
        ];

        if (in_array($this->defaultKey, $requiredFields) || in_array('', $requiredFields)) {
            return false;
        }

        return true;
    }

    /**
     *  required fields
     *  @return array
     * */
    public function item($itemData)
    {

        $mappingService = new MappingService($itemData, $this->lang, $this->defaultKey);

        $requiredFields = $this->requireFields($mappingService);

        if (!$requiredFields) {
            return false;
        }

        $item = [
                 'id' => $this->getItem($mappingService->UnitId,'id'),
                 'url' => $this->getItem($mappingService->UrlSale,'url'),
                 'title' => $this->title($mappingService->Titles, 'th', 'title'),
                 'type' => $this->listingTypes($mappingService->IsSale,  $mappingService->IsRental,  'type'),
                 'content' => $this->descriptions($mappingService->Descriptions, 'th', 'content'),
                 'price' => $this->prices( $mappingService->CurrentPrice,  'price'),
                 'property_type' => $this->typeGroup($mappingService->PropertyTypeTitle, 'property_type'),
                 'address' =>  $this->unsetKey('address'),
                 'floor_number' => $this->unsetKey('floor_number'),
                 'city_area' => $this->getItem($mappingService->LocationName,'city_area'),
                 'city' => $this->getItem($mappingService->City,'city'),
                 'region' => $this->getItem($mappingService->RegionName,'region'),
                 'latitude' => $this->getItem($mappingService->Latitude,'latitude'),
                 'longitude' =>  $this->getItem($mappingService->Longitude,'longitude'),
                 'orientation' => $this->unsetKey('orientation'),
                 'agency' => $this->unsetKey('agency'),
                 'floor_area' => round($this->getItem($mappingService->IndoorArea,'floor_area'), 2),
                 'plot_area' => [
                     '@attributes' => ['unit' => 'feet'],
                     '@value' => round($this->getItem($mappingService->PlotSize,'plot_area'), 2),
                 ],
                 'rooms' => $this->getItem($mappingService->Bedrooms,'rooms'),
                 'bathrooms' => $this->getItem($mappingService->Bathrooms,'bathrooms'),
                 'condition' => $this->unsetKey('condition'),
                 'year' => '<![CDATA['.date('Y').']]>',
                 'virtual_tour' => $this->unsetKey('virtual_tour'),
                 'eco_score' =>  $this->unsetKey('eco_score'),
                 'pictures' => $this->images($mappingService->Images, 'pictures'),
                 'date' =>  '<![CDATA['.date('D, d M Y H:i:s O').']]>',
                 'expiration_date' =>  '<![CDATA['.date('D, d M Y').']]>',
                 'by_owner' =>  $this->unsetKey('by_owner'),
                 'parking' =>  $this->unsetKey('parking'),
                 'is_furnished' =>  $this->unsetKey('is_furnished'),
                 'is_new' =>  $this->unsetKey('is_new'),
        ];


        foreach ($this->forgetKeys as $value) {
            Arr::forget($item, $value);
        }

        $this->forgetKeys = [];

        return  $item;

    }

    /**
     *  unset the item keys which not exists
     *  @param $value
     *  @param $key
     *  @return string
     * */
    public  function getItem($value, $key)
    {
        if (!$value) {
            $this->unsetKey($key);
        }

        return '<![CDATA['.$value.']]>';
    }

    /**
     *  unset the item keys which not exists
     *  @param $key
     *  @return mixed
     * */
    public  function unsetKey($key)
    {
        $this->forgetKeys[] = $key;
    }

    /**
     *  make images as Dot mapping
     *  @param $images
     *  @param $forgetKey
     *  @return array
     * */
    public function images($images, $forgetKey = null)
    {
        $getImages = json_decode($images);

        if (empty($getImages) && $forgetKey) {
            $this->unsetKey($forgetKey);
        }

        $imagesArray = [];
        foreach ($getImages as $key => $image) {
            $arr = [
                'picture_url' => '<![CDATA['.$image.']]>',
                'picture_title' => '<![CDATA[thb]]>'
            ];
            $imagesArray[] = $arr;
        }
        return $imagesArray;
    }

    /**
     *  make titles as Dot mapping
     *  @param $titles
     *  @param $lang
     *  @param $forgetKey
     *  @return array
     * */
    public function title($titles, $lang, $forgetKey)
    {
        $titles = json_decode($titles);
        if (!isset($titles->$lang)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return '<![CDATA['.$titles->$lang.']]>';
    }

    /**
     *  get dd listing types
     * @param  $isSale string
     * @param  $isRental  string
     * @param  $forgetKey  string
     * @return string
     * */
    public function listingTypes($isSale, $isRental, $forgetKey = null)
    {
        if ($isSale && $isRental && $forgetKey) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        if ($isSale == 'yes' ) { return  '<![CDATA[SALE]]>'; };
        if ($isRental == 'yes' ) { return '<![CDATA[RENT]]>'; };
    }

    /**
     *  make descriptions as Dot mapping
     *  @param $descriptions
     *  @param $lang
     *  @param $forgetKey
     *  @return array
     * */
    public function descriptions($descriptions, $lang, $forgetKey)
    {
        $descriptions = json_decode($descriptions);
        if (!isset($descriptions->$lang)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return '<![CDATA['.$descriptions->$lang.']]>';
    }

    /**
     *  make prices as Dot mapping
     *
     *  @param $getSalePrice
     *  @param $forgetKey
     *
     *  @return array
     * */
    public function prices($getSalePrice , $forgetKey = null)
    {

        if ($getSalePrice > 0.00) {
            return [
                '@attributes' => [
                    'currency' => 'THB',
                    'period' => 'monthly'
                ],
                '@value' => '<![CDATA['.$getSalePrice.']]>',
            ];
        }

        if ($forgetKey) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

    }

    /**
     *
     * @param  $typeName string
     * @param  $forgetKey  string
     * @return string
     * */
    public function typeGroup($typeName, $forgetKey)
    {

        if (!isset($this->propertyType[$typeName])) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        return '<![CDATA['.$this->propertyType[$typeName].']]>';
    }


}