<?php

namespace FazWaz\Feed\Mappings;

use Illuminate\Support\Arr;
use FazWaz\Feed\Generate\DataConvert;
use FazWaz\Feed\Services\MappingService;

class HipFlatPropertyMapping {

    /**
     *  It's  folder name when  upload the generated XML file
     * @var $dirName
     * */
    public $dirName = 'property-listing/hipflat';

    /**
     *  node name for XML file
     * @var $nodeName
     * */
    public $nodeName = 'channel';

    /**
     * make child up on nodeName
     * @var $xmlChild
     * */
    public $xmlChild = [
        '@child' =>
            [
                '@name' => 'rss',
                '@attributes' => [
                    'xmlns:atom' => 'https://fazwaz.sgp1.digitaloceanspaces.com/property-listing/hipflat/feed.xml',
                    'version' => '2.0'
                ]
            ]
    ];

    /**
     *  Property field default key if the value is not found
     * @var $defaultKey
     * */
    public $defaultKey = null;

    /**
     *  languages
     * @var $lang
     * */
    public $lang = ['en', 'th'];

    /**
     *
     * @var $forgetKeys array
     * */
    public $forgetKeys = [];

    /**
     *
     * @var $propertyType array
     * */
    public $propertyType = [
        'Townhouse' => 'townhouse',
        'Penthouse' => 'condo',
        'land' => 'land',
        'PrivateIsland' => 'land',
        'Hotel' => 'N/F',
        'Restaurant' => 'N/F',
        'Bar' => 'N/F',
        'Shop' => 'N/F',
        'Office' => 'N/F',
        'Warehouse' => 'N/F',
        'House' => 'house',
        'Villa' => 'house',
        'Condo' => 'condo',
        'Apartment' => 'apartment',
    ];

    /**
     *   array data for HipFlat features map columns
     *  @var $featuresData
     * */
    public  $featuresData = [
        'Media Room/Cinema' => 'studyRoom',
        'Private Gym' => 'gym',
        'Private Sauna' => 'sauna',
        'Private Pool' => 'privatePool',
        'Jacuzzi' => 'jacuzzi',
        'Rooftop Terrace' => 'roofTerrace',
        'Private Garden' => 'privateGarden',
        'Garden Access' => 'garden',
        'Pool Lounge' => 'pool',
        'Terrace' => 'terrace',
        'Covered Parking' => 'garage',
        'Corner Unit' => 'cornerUnit',
        'Golf Membership' => 'miniGolf',
        'Maids Quarters' => 'maidsRoom',
        'Duplex' => 'duplex',
        'Balcony' => 'balcony',
        'Wifi Included' => 'wifi',
        'Bathtub' => 'bathtub',
        'Fully Renovated' => 'renovated',
        'Renovated Kitchen' => 'renovated',
        'Renovated Bathroom' => 'renovated',
    ];

    /**
     *   array data for HipFlat scenery map columns
     *  @var $sceneryData
     * */
    public $sceneryData = [
        'City View' => 'cityView',
        'Garden View' => 'greenView',
        'Lake View' => 'lakeView',
        'Mountain View' => 'mountainView',
        'Pool View' => 'poolView',
        'Sea View' => 'seaView',
        'River View' => 'riverView',
    ];

    /**
     *  Headers static data
     *  @return array
     * */
    public function header () {
        return [
            'title' => 'Thailand Property | 50,000+ Condos & Villas | Sale & Rent | FazWaz',
            'description' => 'Search through Thailand\'s largest and most popular property platform. FazWaz compares over 50,000 condos & villas using market data and analytics.',
            'link' => '​https://www.fazwaz.com',
            'lastBuildDate' => date('D, d M Y H:i:s O') ,
            'pubDate' => date('D, d M Y H:i:s O'),
            'atom:link' => [
                '@attributes' => [
                    'href' => 'https://fazwaz.sgp1.digitaloceanspaces.com/property-listing/hipflat/feed.xml',
                    'rel' => 'self',
                    'type' => 'application/rss+xml'
                ]
            ]
        ];
    }

    /**
     *  Set Data for convert array and mapping data with that converted  array
     *  @param  $type string
     *  @param  $data array
     *  @return array
     *  @throws
     * */
    public function mappingData($type, $data) {

        $dataConvert = new DataConvert();

        $convertedArray = $dataConvert->makeArray($type, $data);

        if ($convertedArray['status_code'] != 200) {
            return $convertedArray;
        }

        $newData = [];

        foreach ($convertedArray['data'] as $key => $itemData)
        {
            $mappedPropertyData = $this->item($itemData);

            if ($mappedPropertyData) {
                $newData[$key] = $mappedPropertyData;
            }

        }

        return [
            'status_code' => 200,
            'data' => $this->xmlChild + $this->header() + ['item' => $newData]
        ];

    }

    /**
     *  Mapping json data to array
     *  @param  $mappingService object
     *  @return boolean
     * */
    public function requireFields($mappingService)
    {
        $requiredFields =  [
//            'propertyType' => $this->typeGroup($mappingService->PropertyTypeTitle, 'propertyType'),
            'region' => $mappingService->RegionName,
//            'status' => 'available',
//            'beds' => $mappingService->Bedrooms,
//            'baths' => $mappingService->Bathrooms,
//            'area' => $mappingService->IndoorArea,
        ];

        if (in_array($this->defaultKey, $requiredFields) || in_array('', $requiredFields)) {
            return false;
        }

        return true;
    }

    /**
     *  required fields
     *  @param  $itemData
     *  @return array
     * */
    public function item($itemData)
    {
        $mappingService = new MappingService($itemData, $this->lang, $this->defaultKey);

        $requiredFields = $this->requireFields($mappingService);

        if (!$requiredFields) {
            return false;
        }

        $item = [
            'status' => 'available',
            'propertyType' =>  $this->typeGroup($mappingService->PropertyTypeTitle, 'propertyType'),
            'title' => $this->title($mappingService->Titles, 'en', 'title'),
            'description' => $this->descriptions($mappingService->Descriptions, 'en', 'description'),
            'projectName' => $this->getItem($mappingService->ProjectName,'projectName'),
            'link' => $this->getItem($mappingService->UrlSale,'link'),
            'refId' =>  $this->getItem($mappingService->UnitCode,'refId'),
            'published' => $this->unsetKey('published'),
            'updated' => $this->unsetKey('updated'),
            'region' => $this->getItem($mappingService->RegionName,'region'),
            'subregion' => $this->getItem($mappingService->City,'subregion'),
            'coordinates' => [
                'lat' =>  $this->getItem($mappingService->Latitude,'coordinates.lat'),
                'lng' => $this->getItem($mappingService->Longitude,'coordinates.lng'),
            ],
            'freeformLocation' => $this->getItem($mappingService->LocationName,'freeformLocation'),
            'place' => $this->unsetKey('place'),
            'station' => [
                '@attributes' => ['category' => 'bts'],
                '@value' => $this->unsetKey('station'),
            ],
            'shortTermRent'  =>  [
                'dailyPrice'   => $this->unsetKey('shortTermRent.dailyPrice'),
                'weeklyPrice'  => $this->unsetKey('shortTermRent.weeklyPrice'),
                'monthlyPrice' => $this->unsetKey('shortTermRent.monthlyPrice'),
            ],
            'salePrice' =>  $this->getItem($mappingService->CurrentPrice,'salePrice'),
            'rentPrice' => $this->getItem($mappingService->YearlyRentalPrice,'rentPrice'),
            'beds' =>  $this->getItem($mappingService->Bedrooms,'beds'),
            'baths'  =>  $this->getItem($mappingService->Bathrooms,'baths'),
            'area' => round($this->getItem($mappingService->IndoorArea,'area'), 2),
            'floor' => $this->getItem($mappingService->Floor,'floor'),
            'plotArea' => round($this->getItem($mappingService->PlotSize,'plotArea'), 2),
            'parkingSpaces' => $this->unsetKey('parkingSpaces'),
            'photos' =>  $this->images($mappingService->Images, 'photos') , // photos,
            'features'=> $this->explodeData($mappingService->Feature, 'features', $this->featuresData),
            'scenery' => $this->explodeData($mappingService->Scenery, 'scenery', $this->sceneryData),
        ];

        foreach ($this->forgetKeys as $value) {
            Arr::forget($item, $value);
        }

        $this->forgetKeys = [];

        return  $item;
    }


    /**
     *  unset the item keys which not exists
     *  @param $value
     *  @param $key
     *  @return string
     * */
    public  function getItem($value, $key)
    {
        if (!$value) {
            $this->unsetKey($key);
        }

        return $value;
    }

    /**
     *  unset the item keys which not exists
     *  @param $key
     *  @return void
     * */
    public  function unsetKey($key)
    {
        $this->forgetKeys[] = $key;
    }

    /**
     *
     * @param  $typeName string
     * @param  $forgetKey  string
     * @return string
     * */
    public function typeGroup($typeName, $forgetKey)
    {

        if (!isset($this->propertyType[$typeName])) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        return $this->propertyType[$typeName];
    }

    /**
     *  make titles as Dot mapping
     *  @param $titles
     *  @param $lang
     *  @param $forgetKey
     *  @return array
     * */
    public function title($titles, $lang, $forgetKey)
    {
        $titles = json_decode($titles);
        if (!isset($titles->$lang)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $titles->$lang;
    }

    /**
     *  make descriptions as Dot mapping
     *  @param $descriptions
     *  @param $lang
     *  @param $forgetKey
     *  @return array
     * */
    public function descriptions($descriptions, $lang, $forgetKey)
    {
        $descriptions = json_decode($descriptions);
        if (!isset($descriptions->$lang)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $descriptions->$lang;
    }

    /**
     *  make images as Dot mapping
     *  @param $images
     *  @param $forgetKey
     *  @return array
     * */
    public function images($images, $forgetKey = null)
    {
        $getImages = json_decode($images);

        if (empty($getImages) && $forgetKey) {
            $this->unsetKey($forgetKey);
        }

        foreach ($getImages as $key => $image) {
            $imagesArray[] = $image;
        }

        return [
            'photo' => $imagesArray
        ];
    }


    /**
     *  merge array's
     * @param  $data string
     * @param  $forgetKey  string
     * @param  $allData  array
     * @return array
     * */
    public function  explodeData($data, $forgetKey, $allData)
    {

        $explodeToArr = explode(",", $data);

        $arrayInter = array_intersect_key($allData, array_flip($explodeToArr));

        $elements = [];
        foreach ($arrayInter as $keyInter => $valueInter) {
            $elements[$valueInter] = true;
        }

        if (empty($elements)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        return $elements;
    }


}