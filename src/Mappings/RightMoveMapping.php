<?php

namespace FazWaz\Feed\Mappings;

use Illuminate\Support\Arr;
use FazWaz\Feed\Generate\DataConvert;
use FazWaz\Feed\Services\MappingService;

class RightMoveMapping
{

    /**
     *  It's  folder name when  upload the generated XML file
     * @var $dirName
     * */
    public $dirName = 'property-listing/rightmove';

    /**
     *  node name for XML file
     * @var $nodeName
     * */
    public $nodeName = 'overseasAddUpdatePropertyForm';

    /**
     *  Property field default key if the value is not found
     * @var $defaultKey
     * */
    public $defaultKey = null;

    /**
     *  languages
     * @var $lang
     * */
    public $lang = ['en', 'th'];

    /**
     *
     * @var $forgetKeys array
     * */
    public $forgetKeys = [];

    /**
     *
     * @var $propertyType array
     * */
    public $propertyType = [
        'Townhouse' => 'townhouse',
        'Penthouse' => 'condo',
        'land' => 'land',
        'PrivateIsland' => 'land',
        'Hotel' => 'N/F',
        'Restaurant' => 'N/F',
        'Bar' => 'N/F',
        'Shop' => 'N/F',
        'Office' => 'N/F',
        'Warehouse' => 'N/F',
        'House' => 'house',
        'Villa' => 'villa',
        'Condo' => 'condo',
        'Apartment' => 'apartment',
    ];

    /**
     *   array data for HipFlat scenery map columns
     *  @var $sceneryData
     * */
    public $sceneryData = [
        'Golf Course' => 'golf_course_on_site_or_within_10_minutes_walk',
        'Garden View' => 'private_pool',
        'Lake View' => 'at_beach_or_within_10_minute_walk',
        'Mountain View' => 'air_conditioning',
        'Pool View' => 'pool_view',
        'Sea View' => 'sea_view',
        'Security System' => 'security_system',
        'Gated Entry View' => 'gated_entry',
        'Balcony' => 'balcony',
        'Ground Floor Terrace' => 'ground_floor_terrace',
        'Roof Terrace' => 'roof_terrace',
        'Hot Tub' => 'hot_tub',
        'Log Fireplace' => 'log_fireplace',
        'Business For Sale' => 'business_for_sale',
    ];

    /**
     *  Return Static Header data
     * @return array
     * */
    public function header()
    {
        return [
            'network' => [
                'network_id' => 234,
            ],
            'branch' => [
                'branch_id' => 5653,
            ]

        ];
    }

    /**
     *  Set Data for convert array and mapping data with that converted  array
     * @param  $type string
     * @param  $data array
     * @return array
     * @throws
     * */
    public function mappingData($type, $data)
    {

        $dataConvert = new DataConvert();

        $convertedArray = $dataConvert->makeArray($type, $data);

        if ($convertedArray['status_code'] != 200) {
            return $convertedArray;
        }

        $newData = [];

        foreach ($convertedArray['data'] as $key => $itemData) {
            $mappedPropertyData = $this->item($itemData);

            if ($mappedPropertyData) {
                $newData[$key] = $mappedPropertyData;
            }
        }

        return [
            'status_code' => 200,
            'data' => [
                'property' => $newData
            ]
        ];

    }


    /**
     *  Mapping json data to array
     * @param  $mappingService object
     * @return boolean
     * */
    public function requireFields($mappingService)
    {
        $requiredFields = [
//          'network',
//          'url' ,
//          'title' ,
//          'type'  ,
//          'content' ,
//          'country_code',
//          'price' ,
//          'description' ,
//          'bedrooms'  ,
        ];

        if (in_array('N/F', $requiredFields) || in_array('', $requiredFields)) {
            return false;
        }

        return true;
    }

    /**
     *  required fields
     * @param $itemData
     * @return array
     * */
    public function item($itemData)
    {

        $mappingService = new MappingService($itemData, $this->lang, $this->defaultKey);

        $requiredFields = $this->requireFields($mappingService);

        if (!$requiredFields) {
            return false;
        }

        $item = [
            'agent_ref' => $mappingService->UnitCode,
            'published' => $this->unsetKey('published'),
            'property_type' => $this->typeGroup($mappingService->PropertyTypeTitle, 'property_type'),
            'os_status' => 'ACTIVE',
            'create_date' => date('d-m-Y H:i:s'),
            'update_date' => date('d-m-Y H:i:s'),
            'address' => [
                'country_code' => $mappingService->CountryName ? $mappingService->CountryName : $this->unsetKey('address.country_code'),
                'region' => $mappingService->RegionName ? $mappingService->RegionName : $this->unsetKey('address.region'),
                'sub_region' => $mappingService->City ? $mappingService->City : $this->unsetKey('address.sub_region'),
                'town_city' => $mappingService->LocationName ? $mappingService->LocationName : $this->unsetKey('address.town_city'),
                'latitude' => $mappingService->Latitude ? $mappingService->Latitude : $this->unsetKey('address.latitude'),
                'longitude' => $mappingService->Longitude ? $mappingService->Longitude : $this->unsetKey('address.longitude'),
            ],
            'price_information' => [
                'price' => $mappingService->CurrentPrice ? $mappingService->CurrentPrice : $this->unsetKey('price_information.price'),
                'os_price_qualifier' => $this->unsetKey('price_information.os_price_qualifier'),
            ],
            'details' => [
                'summary' => $this->unsetKey('details.summary'),
                'description' => $this->descriptions($mappingService->Descriptions, 'en', 'description'),
                'features' => $this->features($mappingService->Feature, 'features'),
                'bedrooms' =>   $mappingService->Bedrooms ? $mappingService->Bedrooms : $this->unsetKey('details.bedrooms'),
                'bathrooms' =>   $mappingService->Bathrooms ? $mappingService->Bathrooms : $this->unsetKey('details.bathrooms'),
                'year_built' =>   $this->unsetKey('details.year_built'),
                'internal_area' =>   $mappingService->PlotSize  ? round($mappingService->PlotSize, 2) : $this->unsetKey('details.internal_area'),
                'internal_area_unit' =>   $this->unsetKey('details.internal_area_unit'),
                'land_area' =>   $mappingService->IndoorArea ? round($mappingService->IndoorArea, 2) : $this->unsetKey('details.land_area'),
                'land_area_unit' =>   $this->unsetKey('details.land_area_unit'),
                'entrance_floor' =>    $this->unsetKey('details.entrance_floor'),
                'condition' =>    $this->unsetKey('details.condition'),
                'accessibility' =>    $this->unsetKey('details.accessibility'),
                'scenery' => $this->explodeData($mappingService->Scenery, 'scenery', $this->sceneryData),
            ],
            'media' =>  $mappingService->Images  ? $this->images($mappingService->Images) : $this->unsetKey('media'),

        ];

        foreach ($this->forgetKeys as $value) {
            Arr::forget($item, $value);
        }

        $this->forgetKeys = [];

        return  $item;
    }

    /**
     *  unset the item keys which not exists
     * @param $key
     * @return void
     * */
    public function unsetKey($key)
    {
        $this->forgetKeys[] = $key;
    }

    /**
     *
     * @param  $typeName string
     * @param  $forgetKey  string
     * @return string
     * */
    public function typeGroup($typeName, $forgetKey)
    {
        $data = strtolower($typeName);
        if (!isset($this->propertyType[$data])) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $this->propertyType[$data];
    }

    /**
     *  make descriptions as Dot mapping
     * @param $descriptions
     * @param $lang
     * @param $forgetKey
     * @return array
     * */
    public function descriptions($descriptions, $lang, $forgetKey)
    {
        $descriptions = json_decode($descriptions);
        if (!isset($descriptions->$lang)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $descriptions->$lang;
    }

    /**
     *  merge array's
     * @param  $data string
     * @param  $forgetKey  string
     * @return array
     * */
    public function features($data, $forgetKey)
    {

        $explodeToArr = explode(",", $data);
        if (empty($explodeToArr)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $explodeToArr;
    }

    /**
     *  merge array's
     * @param  $data string
     * @param  $forgetKey  string
     * @param  $allData  array
     * @return array
     * */
    public function  explodeData($data, $forgetKey, $allData)
    {

        $explodeToArr = explode(",", $data);

        $arrayInter = array_intersect_key($allData, array_flip($explodeToArr));

        $elements = [];
        foreach ($arrayInter as $keyInter => $valueInter) {
            $elements[$valueInter] = 1;
        }

        if (empty($elements)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        return $elements;
    }

    /**
     *  make images as Dot mapping
     *  @param $images
     *  @param $forgetKey
     *  @return array
     * */
    public function images($images, $forgetKey = null)
    {
        $getImages = json_decode($images);

        if (empty($getImages) && $forgetKey) {
            $this->unsetKey($forgetKey);
        }

        $imagesArray = [];
        foreach ($getImages as $key => $image) {
            $arr = [
                'media_type' => 'jpg',
                'media_url' => $image
            ];
            $imagesArray[] = $arr;
        }
        return $imagesArray;
    }

}