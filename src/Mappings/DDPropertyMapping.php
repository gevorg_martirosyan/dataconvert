<?php

namespace FazWaz\Feed\Mappings;

use Exception;
use FazWaz\Feed\Generate\DataConvert;
use FazWaz\Feed\Services\MappingService;
use Illuminate\Support\Arr;

class DDPropertyMapping {

    /**
     *  It's  folder name when  upload the generated XML file
     * @var $dirName
     * */
    public $dirName = 'property-listing/ddproperty';

    /**
     *  node name for XML file
     * @var $nodeName
     * */
    public $nodeName = 'listing-data';

    /**
     *  Property field default key if the value is not found
     * @var $defaultKey
     * */
    public $defaultKey = null;

    /**
     *  languages
     * @var $lang
     * */
    public $lang = ['en', 'th'];

    /**
     *
     * @var $forgetKeys array
     * */
    public $forgetKeys = [];

    /**
     * @var $convertLocationUrl
     * */
    protected $convertLocationUrl = 'files/ddpropertylocation.json';

    /**
     *  get property types group with
     *  requirement data nested by type
     *  @var $getPropertyTypeGroup
     * */
    public $getPropertyTypeGroup = [
        'condo' =>  [
            '@value' => 'N',
            '@requirements' => [ 'IndoorArea'],
        ],
        'house' =>  [
            '@value' => 'B',
            '@requirements' => [ 'PlotSize'],
        ],
        'townhouse' =>  [
            '@value' => 'T',
            '@requirements' => [ 'PlotSize'],
        ],
        'apartment' =>  [
            '@value' => 'A',
            '@requirements' => [ 'PlotSize'],
        ],
        'land' =>  [
            '@value' => 'L',
            '@requirements' => [ 'PlotSize'],
        ],
        'office' =>  [
            '@value' => 'OFF',
            '@requirements' => [ 'IndoorArea'],
        ],
        'retail space' =>  [
            '@value' => 'RET',
            '@requirements' => [ 'IndoorArea'],
        ],
        'shophouse/commercial property' =>  [
            '@value' => 'SHOP',
            '@requirements' => [ 'PlotSize'],
        ],
        'warehouse/factory' =>  [
            '@value' => 'WAR',
            '@requirements' => [ 'PlotSize'],
        ],
        'business' =>  [
            '@value' => 'BIZ',
            '@requirements' => [ 'PlotSize'],
        ]
    ];

    /**
     *   array data for HipFlat features map columns
     *  @var $featuresData
     * */
    public  $featuresData = [
        'Private Pool' => 'PPOOL',
        'Jacuzzi' => 'JACZ',
        'Rooftop Terrace' => 'ROOF',
        'Private Garden' => 'OPLAN',
        'Terrace' => 'TERR',
        'Covered Parking' => 'GAR',
        'Corner Unit' => 'CORN',
        'Maids Quarters' => 'MAID',
        'Balcony' => 'BAL',
        'Wifi Included' => 'ISDN',
        'Full Western Kitchen' => 'KITCH',
        'Bathtub' => 'BATH',
        'Fully Renovated' => 'RENO',
        'Renovated Kitchen' => 'RENO',
        'Renovated Bathroom' => 'RENO',
    ];

    /**
     *   array data for HipFlat scenery map columns
     *  @var $sceneryData
     * */
    public $sceneryData = [
        'City View' => 'SEAV',
        'Garden View' => 'GREEN',
        'Pool View' => 'POOLV',
        'Sea View' => 'SEAV',
    ];

    /**
     *  Set Data for convert array and mapping data with that converted  array
     *  @param  $type string
     *  @param  $data array
     *  @return array
     *  @throws Exception
     * */
    public function mappingData($type, $data) {

        $dataConvert = new DataConvert();

        $convertedArray = $dataConvert->makeArray($type, $data);

        if ($convertedArray['status_code'] != 200) {
            return $convertedArray;
        }


        $newData = [];

        foreach ($convertedArray['data'] as $key => $itemData)
        {
            $mappedPropertyData = $this->item($itemData);

            if ($mappedPropertyData) {
                $newData[$key] = $mappedPropertyData;
            }

        }

        return [
            'status_code' => 200,
            'data' => [
                'property' => $newData
            ]
        ];

    }

    /**
     *  Mapping json data to array
     *  @param  $itemData array
     *  @return boolean
     * */
    public function requireFields($mappingService)
    {

        $requiredFields = [
            'ref-no' => $mappingService->UnitCode,
//            'title' => $this->title($mappingService->Titles, 'th', 'details.title'),
//            'description' => $this->descriptions($mappingService->Descriptions, 'th', 'details.description'),
        ];

        //property types requirements
        $propertyType = $this->typeGroup($mappingService->PropertyTypeTitle, 'location.property-type-group');
        if ($propertyType) {
            $requiredFields['property-type-group'] = $propertyType['@value'];

            foreach ($propertyType['@requirements'] as $field) {
                $requiredFields[$field] = $mappingService->$field;
            }
        } else {
            $requiredFields['property-type-group'] = $this->defaultKey;
        }

        // take lng & lat
        $lng =  $mappingService->Longitude;
        $lat =  $mappingService->Latitude;

        // not required if longitude and latitude tags of such listing are filled
        if (!$lng || !$lat) {

            $requiredFields['area-code'] = $this->convertLocation($mappingService->LocationName, 'location.area-code', 'a');
            $requiredFields['district-code'] =  $this->convertLocation($mappingService->City, 'location.district-code', 'd');
            $requiredFields['region-code'] = $this->convertLocation($mappingService->RegionName,'location.region-code', 'l');
        }


        if (in_array($this->defaultKey, $requiredFields) || in_array('', $requiredFields)) {
            return false;
        }

        return true;
    }

    /**
     *  required fields
     *  @param $itemData
     *  @return array
     * */
    public function item($itemData)
    {
        $mappingService = new MappingService($itemData, $this->lang, $this->defaultKey);

        $requiredFields = $this->requireFields($mappingService);

        if (!$requiredFields) {
            return false;
        }

        $item = [
            'ref-no' => $this->getItem($mappingService->UnitCode,'ref-no'), // required
            'location' =>  [
                'streetname' => $this->getItem($mappingService->Street,'location.streetname'),
                'streetnumber' =>  $this->unsetKey('location.streetnumber'),
                'post-code' => $this->getItem($mappingService->Zip,'location.post-code'),
                'area-code' =>  $this->convertLocation($mappingService->LocationName, 'location.area-code', 'a'),
                'district-code' => $this->convertLocation($mappingService->City, 'location.district-code', 'd'),
                'region-code' => $this->convertLocation($mappingService->RegionName,'location.region-code', 'l'),
                'longitude' => $this->getItem($mappingService->Latitude,'location.longitude'),
                'latitude' => $this->getItem($mappingService->Longitude,'location.latitude'),
                'property-name' => $this->getItem($mappingService->ProjectName,'location.property-name'),
                'property-type-group' => $this->typeGroup($mappingService->PropertyTypeTitle, 'location.property-type-group'),
            ],
            'details' =>  [
                'title' => $this->title($mappingService->Titles, 'th', 'details.title'),
                'title_en' => $this->title($mappingService->Titles, 'en', 'details.title_en'),
                'description' => $this->descriptions($mappingService->Descriptions, 'th', 'details.description'),
                'description_en' => $this->descriptions($mappingService->Descriptions, 'en', 'details.description_en'),
                'features' => $this->explodeData($mappingService->Feature, 'details.features', $this->featuresData),
                'amenities' => $this->explodeData($mappingService->Scenery, 'details.amenities', $this->sceneryData),
                'price-details' => $this->prices($mappingService->IsSale,  $mappingService->IsRental, $mappingService->CurrentPrice, $mappingService->YearlyRentalPrice, 'details.price-details'),
                'rooms' => [
                    'num-bedrooms' =>$this->getItem($mappingService->Bedrooms,'details.rooms.num-bedrooms'),
                    'extra-rooms' => $this->unsetKey('details.rooms.extra-rooms'),
                    'num-bathrooms' => $this->getItem($mappingService->Bathrooms,'details.rooms.num-bathrooms'),
                ],
                'furnishing' => $this->unsetKey('details.furnishing'),
                'size-details' => [
                    'floor-area'  => round($this->getItem($mappingService->IndoorArea,'details.size-details.floor-area'), 2),
                    'land-area' => round($this->getItem($mappingService->PlotSize,'details.size-details.land-area'), 2),
                ],
                'numberoffloors' => $this->getItem($mappingService->Floor,'details.numberoffloors'),
                'floor-level'  => $this->unsetKey('details.floor-level'),
            ],
            'listing-type' => $this->listingTypes($mappingService->IsSale,  $mappingService->IsRental,  'listing-type'),
            'custom-name' =>  $this->unsetKey('custom-name'),
            'custom-phone' => $this->unsetKey('custom-phone'),
            'custom-mobile' => $this->unsetKey('custom-mobile'),
            'custom-email' => $this->unsetKey('custom-email'),
            'tenure' => $this->unsetKey('tenure'),
            'sold' => $this->unsetKey('sold'),
            'status' => 'ACTIVE',
            'source-url' => $this->unsetKey('source-url'),
            'photo' => $this->images($mappingService->Images, 'photo'), // photos
        ];

        foreach ($this->forgetKeys as $value) {
            Arr::forget($item, $value);
        }

        $this->forgetKeys = [];

        return  $item;
    }

    /**
     *  unset the item keys which not exists
     *  @param $value
     *  @param $key
     *  @return string
     * */
    public  function getItem($value, $key)
    {
        if (!$value) {
            $this->unsetKey($key);
        }

        return $value;
    }

    /**
     *  unset the item keys which not exists
     *  @param $key
     *  @return void
     * */
    public  function unsetKey($key)
    {
        $this->forgetKeys[] = $key;
    }


    /**
     *  make images as Dot mapping
     *  @param $images
     *  @param $forgetKey
     *  @return array
     * */
    public function images($images, $forgetKey = null)
    {
        $getImages = json_decode($images);

        if (empty($getImages) && $forgetKey) {
            $this->unsetKey($forgetKey);
        }

        foreach ($getImages as $key => $image) {
            $arr = [
                'picture-url' => $image,
                'picture-caption' => 'thb'
            ];
            $imagesArray[] = $arr;
        }

        return $imagesArray;
    }

    /**
     *  get dd listing types
     * @param  $isSale string
     * @param  $isRental  string
     * @param  $forgetKey  string
     * @return string
     * */
    public function listingTypes($isSale, $isRental, $forgetKey = null)
    {
        if ($isSale && $isRental && $forgetKey) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        if ($isRental == 'yes' && $isSale  == 'yes') { return 'OPT'; }
        if ($isSale == 'yes' ) { return  'SALE'; };
        if ($isRental == 'yes' ) { return 'RENT'; };

    }

    /**
     *  Mapping json data to array
     * @param  $isSale array
     * @param  $isRental string
     * @param  $currentPrice string
     * @param  $rentalPrice string
     * @param $forgetKey string
     * @return array
     * */
    public function prices($isSale, $isRental, $currentPrice, $rentalPrice, $forgetKey)
    {
        $listingType = $this->listingTypes($isSale, $isRental);

        if (!$listingType) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        switch ($listingType) {
            case 'SALE':
                $val = $currentPrice;
                break;
            case 'RENT':
                $val = $rentalPrice;
                break;
            case 'OPT':
                $val = $currentPrice;
                break;
            default:
                $this->unsetKey($forgetKey);
                return $this->defaultKey;
                break;
        }

        return [
            'price' => $val,
            'currency-code' => 'thb',
        ];
    }

    /**
     *  merge array's
     * @param  $data string
     * @param  $forgetKey  string
     * @param  $allData  array
     * @return array
     * */
    public function  explodeData($data, $forgetKey, $allData)
    {

        $explodeToArr = explode(",", $data);

        $arrayInter = array_intersect_key($allData, array_flip($explodeToArr));

        $elements = [];
        foreach ($arrayInter as $keyInter => $valueInter) {
            $elements[] = $valueInter;
        }

        if (empty($elements)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        return implode(',', $elements);
    }

    /**
     *  make titles as Dot mapping
     *  @param $titles
     *  @param $lang
     *  @param $forgetKey
     *  @return array
     * */
    public function title($titles, $lang, $forgetKey)
    {
        $titles = json_decode($titles);
        if (!isset($titles->$lang)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $titles->$lang;
    }


    /**
     *  make descriptions as Dot mapping
     *  @param $descriptions
     *  @param $lang
     *  @param $forgetKey
     *  @return array
     * */
    public function descriptions($descriptions, $lang, $forgetKey)
    {
        $descriptions = json_decode($descriptions);
        if (!isset($descriptions->$lang)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $descriptions->$lang;
    }

    /**
     *
     * @param  $typeName string
     * @param  $forgetKey  string
     * @return string
     * */
    public function typeGroup($typeName, $forgetKey)
    {
        $typeName = str_replace(['penthouse', 'villa'], ['condo', 'house'], strtolower($typeName));
        if (!isset($this->getPropertyTypeGroup[$typeName])) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
        return $this->getPropertyTypeGroup[$typeName];
    }

    /**
     *  Mapping json data to array
     *
     * @param  $name string
     * @param  $forgetKey string
     * @param $ddKey string
     *
     * @return array
     * */
    public function convertLocation($name, $forgetKey, $ddKey = null)
    {
        if ($ddKey === null) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        if ($name == 'N/F' || $name == null || empty($name)) {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }

        $locationData = file_get_contents($this->convertLocationUrl);

        $locationData = json_decode($locationData, true);

        $name = $ddKey . strtolower(str_replace(' ', '', $name));

        if (!empty($locationData[$name])) {
            return $locationData[$name]['code'];
        } else {
            $this->unsetKey($forgetKey);
            return $this->defaultKey;
        }
    }

}