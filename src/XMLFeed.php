<?php

namespace FazWaz\Feed;

use FazWaz\Feed\Upload;
use Rakit\Validation\Validator;
use FazWaz\Feed\Generate\ArrayToXML;

class XMLFeed {

    /**
     * Object of arrayToXML class
     *
     * @var arrayToXML
     */
    private $arrayToXML;

    /**
     * Object of Upload class
     *
     * @var $upload
     */
    private $upload;

    /**
     * Object of LocationMapping class
     *
     * @var $validator
     */
    private $validator;

    /**
     * Object of  Classes
     *
     * @var $mappingObject
     */
    private $mappingObject;

    /**
     *  The name directory name related with dependency class
     *
     * @var $mappingObjectDirName string
     */
    public $mappingObjectDirName;

    /**
     * name for generate xml node
     *
     * @var $mappingObjectNodeName string
     */
    public $mappingObjectNodeName;

    /**
     * XMLFeed constructor.
     * @param $mappingObject array
     */
    public function __construct($mappingObject)
    {

        $this->mappingObject = $mappingObject;
        $this->mappingObjectDirName = $mappingObject->dirName;
        $this->mappingObjectNodeName = $mappingObject->nodeName;


        $this->arrayToXML = new ArrayToXML();
        $this->upload = new Upload();
        $this->validator = new Validator();
    }

    /**
     * mapping dotProperty data
     * @param array $data
     * @return array
     * */
    public function mapping($data)
    {

        $validator = $this->validator->make($data, [
            'data' => 'required',
            'data_type' => 'required',
        ]);
        $validator->validate();

        if ($validator->fails()) {
            return [
                'message' => $validator->errors()->toArray(),
                'status_code' => 422
            ];
        }

        return   $this->mappingObject->mappingData($data['data_type'], $data['data']);

    }

    /**
     * generate xml
     * @param  $items
     * @param $nodeName
     *
     * @return array
     * @throws \Exception
     * */
    public function generateArrayToXML($items, $nodeName)
    {
        return $this->arrayToXML->createXML($nodeName, $items);
    }

    /**
     *  upload file to S3
     * @return array
     * */
    public function uploadFile($data)
    {
        $validator = $this->validator->make($data, [
            'region' => 'required',
            'version' => 'required',
            'key' => 'required',
            'secret' => 'required',
            'bucketName' => 'required',
            'filePath' => 'required',
            'fileName' => 'required',
        ]);
        $validator->validate();

        if ($validator->fails()) {
            return [
                'status_code' => 422,
                'message' => $validator->errors()->toArray(),

            ];
        }

        $configs = [
            'region' => $data['region'],
            'version' => $data['version'],
            'credentials' => [
                'key'    => $data['key'],
                'secret' => $data['secret'],
            ],
            'endpoint' => $data['endpoint'],
        ];

        return $this->upload->uploadFile($configs, $data['bucketName'], $data['filePath'], $data['dirName'], $data['fileName']);
    }

}