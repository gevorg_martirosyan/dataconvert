<?php

namespace FazWaz\Feed;

use Aws\S3\S3Client;
use Aws\S3\Exception\S3Exception;

class Upload
{

    /*  upload to S3
     *  @param $path string
     */
    public function uploadFile($configs, $bucketName, $path, $dirName, $fileName)
    {
        try {

            $s3Client = new S3Client($configs);
            $name = $dirName.'/'.$fileName.'.xml';

            $put = $s3Client->putObject([
                'Bucket' => $bucketName,
                'Key'    => $name,
                'Body' => file_get_contents($path),
                'ACL'    => 'public-read',
                'ContentType' => 'application/xml'
            ]);

            return [
                'status_code' => 200,
                'data' =>  [
                    'filePath' => $put['ObjectURL']
                ]
            ];
        } catch (S3Exception $e) {

            // Catch an S3 specific exception.
            echo $e->getMessage();
            die();
        }



    }
}