<?php

namespace FazWaz\Feed\Generate;

use DOMDocument;
use DOMNode;
use Exception;


class ArrayToXML
{
    /**
     * @var string
     */
    private static $encoding = 'UTF-8';

    /**
     * @var $xml|null
     */
    private static $xml = null;

    /**
     * Convert an Array to XML.
     *
     * @param string $nodeName - name of the root node to be converted
     * @param array  $arr  - array to be converted
     *
     * @return array
     * @throws Exception
     */
    public static function createXML($nodeName, $arr = [])
    {
        $xml = self::getXMLRoot();


        // make rss
        if (array_key_exists('@child', $arr) && is_array($arr['@child'])) {
            $rss = $xml->createElement($arr['@child']['@name']);
            $rssNode = $xml->appendChild($rss);
            foreach ($arr['@child']['@attributes'] as $key => $val) {
                $rssNode->setAttribute($key, self::bool2str($val));
            }
            unset($arr['@child']);
            $rssNode->appendChild(self::convert($nodeName, $arr));
        } else {
            $xml->appendChild(self::convert($nodeName, $arr));
        }

        self::$xml = null; // clear the xml node in the class for 2nd time use.

        $dir = 'files/generated';
        if( is_dir($dir) === false )
        {

            mkdir($dir, 0777, true);

        }
        $path = $dir.'/file_xml_'.time().'.xml';
        $xml->save($path) ;

        return  [
            'status_code' => 200,
            'file_name' => $path
        ];
    }

    /**
     * Initialize the root XML node [optional].
     *
     * @param string $version
     * @param string $encoding
     * @param bool   $standalone
     * @param bool   $formatOutput
     */
    private static function init($version = '1.0', $encoding = 'utf-8', $standalone = false, $formatOutput = false)
    {
        self::$xml = new DomDocument($version, $encoding);
        self::$xml->xmlStandalone = $standalone;
        self::$xml->formatOutput = $formatOutput;
        self::$encoding = $encoding;
    }

    /**
     * Get the root XML node, if there isn't one, create it.
     *
     * @return DomDocument|null
     */
    private static function getXMLRoot()
    {
        if (empty(self::$xml)) {
            self::init();
        }
        return self::$xml;
    }

    /**
     * 0Get string representation of boolean value.
     *
     * @param mixed $v
     *
     * @return string
     */
    private static function bool2str($v)
    {
        //convert boolean to text value.
        $v = $v === true ? 'true' : $v;
        $v = $v === false ? 'false' : $v;
        return $v;
    }

    /**
     * Check if the tag name or attribute name contains illegal characters
     * Ref: http://www.w3.org/TR/xml/#sec-common-syn.
     *
     * @param string $tag
     *
     * @return bool
     */
    private static function isValidTagName($tag)
    {
        $pattern = '/^[a-z_]+[a-z0-9\:\-\.\_]*[^:]*$/i';
        return preg_match($pattern, $tag, $matches) && $matches[0] == $tag;
    }

    /**
     * Convert an Array to XML.
     *
     * @param string $nodeName - name of the root node to be converted
     * @param array  $arr       - array to be converted
     *
     * @return DOMNode
     *
     * @throws Exception
     */
    private static function convert( $nodeName, $arr = [] )
    {
        $xml = self::getXMLRoot();

        $node = $xml->createElement($nodeName);

        if (is_array($arr)) {
            // get the attributes first.;
            if (array_key_exists('@attributes', $arr) && is_array($arr['@attributes'])) {
                foreach ($arr['@attributes'] as $key => $value) {
                    if (!self::isValidTagName($key)) {
                        throw new Exception('Illegal character in attribute name. attribute: '.$key.' in node: '.$nodeName);
                    }
                    $node->setAttribute($key, self::bool2str($value));
                }
                unset($arr['@attributes']); //remove the key from the array once done.
            }
            // check if it has a value stored in @value, if yes store the value and return
            // else check if its directly stored as string
            if (array_key_exists('@value', $arr)) {
                $node->appendChild($xml->createTextNode(self::bool2str($arr['@value'])));
                unset($arr['@value']);    //remove the key from the array once done.
                //return from recursion, as a note with value cannot have child nodes.
                return $node;
            }

        }
        //create subnodes using recursion
        if (is_array($arr)) {

            // recurse to get the node for that key
            foreach ($arr as $key => $value) {
                if (!self::isValidTagName($key)) {
                    throw new Exception(' Illegal character in tag name. tag: '.$key.' in node: '.$nodeName);
                }
                if (is_array($value) && is_numeric(key($value))) {
                    // MORE THAN ONE NODE OF ITS KIND;
                    // if the new array is numeric index, means it is array of nodes of the same kind
                    // it should follow the parent key name
                    foreach ($value as $k => $v) {
                        $node->appendChild(self::convert($key, $v));
                    }
                } else {
                    // ONLY ONE NODE OF ITS KIND
                    $node->appendChild(self::convert($key, $value));
                }
                unset($arr[$key]); //remove the key from the array once done.
            }
        }
        // after we are done with all the keys in the array (if it is one)
        // we check if it has any text value, if yes, append it.
        if (!is_array($arr)) {
            $node->appendChild($xml->createTextNode(self::bool2str($arr)));
        }
        return $node;
    }
}

