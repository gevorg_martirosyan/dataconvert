<?php

namespace FazWaz\Feed\Generate;

use Exception;
use Illuminate\Database\Eloquent\Collection;
use FazWaz\Feed\Services\CurlService;

class DataConvert
{


    /**
     *  Make  to array format
     *
     * @param $type string
     * @param $data string|array|object|json
     * @return array
     *
     * @throws Exception
     * */
    public function makeArray($type, $data) {

        $responseData = [];

        switch ($type) {
            case 'url':
                if (filter_var($data, FILTER_VALIDATE_URL) === FALSE) {
                    return [
                        'status_code' => 422,
                        'message'  => 'The data must be valid url',
                    ];
                }

                $responseData = self::url($data);

                 if ($responseData['status_code'] != 200) {
                        return $responseData;
                 }
                $responseData = $responseData['data'];
                break;
            case 'array':

                if (!is_array($data)) {
                    return [
                        'status_code' => 422,
                        'message'  => 'The data must be ARRAY type',
                    ];
                }

                $responseData = $data;

                break;
            case 'object':

                if (!is_object($data)) {
                    return [
                        'status_code' => 422,
                        'message'  => 'The data must be object type',
                    ];
                }

                if ($data instanceof Collection) {
                    $responseData = $data;

                } else {
                    return [
                        'status_code' => 422,
                        'message'  => 'The Object should be model instance',
                    ];
                }

                break;
            case 'json':

                if (!$this->isJSON($data)) {
                    return [
                        'status_code' => 422,
                        'message'  => 'The data must be json type',
                    ];
                }

                $responseData = self::convertToArray($data);

                break;
            default:

                return [
                    'status_code' => 422,
                    'message'  => 'The data must be json, url, array or object type',
                ];
                break;

        }

        return [
            'status_code' => 200,
            'data' => $responseData
        ];
    }

    /**
     *  set url for json
     *
     * @param $url string
     *
     * @return array
     *
     * @throws Exception
     * */
    private static function url($url)
    {

        $cUrl = new CurlService();
        $response = $cUrl->setUrl($url);

        if ($response['status_code'] != 200 ) {

            return [
                'status_code' => $response['status_code'],
                'message'  =>  $response['message'],
            ];
        }

        if ($response['content_type'] != 'application/json' ) {

            return [
                'status_code' => 422,
                'message'  => 'The url content type must be application/json',
            ];

        }

        return [
            'status_code' => 200,
            'data'  =>  self::convertJsonToArray($response['data']),
        ];
    }

    /**
     * Check if the data is json
     *
     * @param $string
     * @return boolean
     * */
    private static function isJSON($string){
        return is_string($string) && is_array(json_decode($string, true)) && (json_last_error() == JSON_ERROR_NONE) ? true : false;
    }

    /**
     * convert json data to array
     *
     * @param $jsonData
     * @return array
     * */
    private static function convertJsonToArray($jsonData)
    {
        return json_decode($jsonData,true);

    }
}