<?php
namespace Fazwaz\Feed\Test;

use FazWaz\Feed\Mappings\TrovitPropertyMapping;
use FazWaz\Feed\XMLFeed;
use \PHPUnit\Framework\TestCase;
use DOMDocument;

class TrovitPropertyMappingTest extends TestCase
{
    /**
     *  Dot Property Mapping
     * tests
     */
    public function test_mapping_trovit_property_success()
    {
        $xmlFeed = new XMLFeed(new TrovitPropertyMapping());

        $dataRequest = [
            'data_type' => 'url',
            'data' => 'https://laravel-frontend.fazwaz.com/properties-20items.json'
        ];

        $result = $xmlFeed->mapping($dataRequest);

        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('status_code', $result);

        $this->assertEquals(200, $result['status_code']);


        $this->assertArrayHasKey(0, $result['data']['ad']);
        $item = $result['data']['ad'][0];

        $this->assertNotEmpty($item);

        // refId
        $this->assertArrayHasKey('id', $item);
        $this->assertNotEmpty($item['id']);

        // propertyType
        $this->assertArrayHasKey('type', $item);
        $this->assertNotEmpty($item['type']);

        // region
        $this->assertArrayHasKey('region', $item);
        $this->assertNotEmpty($item['region']);

        // description
        $this->assertArrayHasKey('content', $item);
        $this->assertNotEmpty($item['content']);


        // pictures []
        $this->assertArrayHasKey('pictures', $item);
        $this->assertNotEmpty($item['pictures']);

        // photo
        $this->assertArrayHasKey(0, $item['pictures']);
        $this->assertNotEmpty($item['pictures'][0]);
        $this->assertArrayHasKey('picture_url', $item['pictures'][0]);
        $this->assertNotEmpty($item['pictures'][0]['picture_url']);
        $this->assertArrayHasKey('picture_url', $item['pictures'][0]);
        $this->assertNotEmpty($item['pictures'][0]['picture_title']);

        // beds
        $this->assertArrayHasKey('bathrooms', $item);
        $this->assertNotEmpty($item['bathrooms']);

    }

}