<?php
namespace Fazwaz\Feed\Test;

use FazWaz\Feed\Mappings\HipFlatPropertyMapping;
use FazWaz\Feed\XMLFeed;
use \PHPUnit\Framework\TestCase;
use DOMDocument;

class HipFlatPropertyMappingTest extends TestCase
{
    /**
     *  Dot Property Mapping
     * tests
     */
    public function test_mapping_dot_property_success()
    {
        $xmlFeed = new XMLFeed(new HipFlatPropertyMapping());

        $dataRequest = [
            'data_type' => 'url',
            'data' => 'https://laravel-frontend.fazwaz.com/properties-20items.json'
        ];

        $result = $xmlFeed->mapping($dataRequest);

        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('status_code', $result);

        $this->assertEquals(200, $result['status_code']);

        $this->assertArrayHasKey(0, $result['data']['item']);
        $item = $result['data']['item'][0];

        $this->assertNotEmpty($item);

        // refId
        $this->assertArrayHasKey('refId', $item);
        $this->assertNotEmpty($item['refId']);

        // propertyType
        $this->assertArrayHasKey('propertyType', $item);
        $this->assertNotEmpty($item['propertyType']);

        // region
        $this->assertArrayHasKey('region', $item);
        $this->assertNotEmpty($item['region']);

        // description
        $this->assertArrayHasKey('description', $item);
        $this->assertNotEmpty($item['description']);


        // photos []
        $this->assertArrayHasKey('photos', $item);
        $this->assertNotEmpty($item['photos']);

        // photo
        $this->assertArrayHasKey('photo', $item['photos']);
        $this->assertNotEmpty($item['photos']['photo']);

        // beds
        $this->assertArrayHasKey('beds', $item);
        $this->assertNotEmpty($item['beds']);

        // baths
        $this->assertArrayHasKey('baths', $item);
        $this->assertNotEmpty($item['baths']);

        // area
        $this->assertArrayHasKey('area', $item);
        $this->assertNotEmpty($item['area']);
    }

}