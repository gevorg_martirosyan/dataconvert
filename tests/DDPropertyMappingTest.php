<?php
namespace Fazwaz\Feed\Test;

use FazWaz\Feed\Mappings\DDPropertyMapping;
use FazWaz\Feed\XMLFeed;
use \PHPUnit\Framework\TestCase;


class DDPropertyMappingTest extends TestCase
{
    /**
     *  Dot Property Mapping
     * tests
     */
    public function test_mapping_dot_property_success()
    {
        $xmlFeed = new XMLFeed(new DDPropertyMapping());

        $dataRequest = [
            'data_type' => 'url',
            'data' => 'https://laravel-frontend.fazwaz.com/properties-20items.json'
        ];

        $result = $xmlFeed->mapping($dataRequest);

        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('status_code', $result);

        $this->assertEquals(200, $result['status_code']);

        $this->assertArrayHasKey(0, $result['data']['property']);
        $item = $result['data']['property'][0];

        $this->assertNotEmpty($item);

        // Ref no (Property ID)
        $this->assertArrayHasKey('ref-no', $item);
        $this->assertNotEmpty($item['ref-no']);

        // location []
        $this->assertArrayHasKey('location', $item);
        $this->assertNotEmpty($item['location']['area-code']);
        $this->assertNotEmpty($item['location']['district-code']);
        $this->assertNotEmpty($item['location']['region-code']);
        $this->assertNotEmpty($item['location']['property-type-group']);


        // details []
        $this->assertArrayHasKey('details', $item);

        // details [description]
        $this->assertArrayHasKey('description', $item['details']);
        $this->assertNotEmpty($item['details']['description']);

        // details [description_en]
        $this->assertArrayHasKey('description_en', $item['details']);
        $this->assertNotEmpty($item['details']['description_en']);

        // details [rooms]
        $this->assertArrayHasKey('rooms', $item['details']);
        $this->assertNotEmpty($item['details']['rooms']);

        // photo []
        $this->assertArrayHasKey('photo', $item);
        $this->assertNotEmpty($item['photo']);

        // photo [0]
        $this->assertArrayHasKey('picture-url', $item['photo'][0]);
        $this->assertArrayHasKey('picture-caption', $item['photo'][0]);
        $this->assertNotEmpty($item['photo'][0]['picture-url']);
        $this->assertNotEmpty($item['photo'][0]['picture-caption']);


    }

}