<?php
namespace Fazwaz\Feed\Test;

use FazWaz\Feed\Mappings\DotPropertyMapping;
use FazWaz\Feed\XMLFeed;
use \PHPUnit\Framework\TestCase;
use DOMDocument;

class DotPropertyMappingTest extends TestCase
{
    /**
     *  Dot Property Mapping
     * tests
     */
    public function test_mapping_dot_property_success()
    {
        $xmlFeed = new XMLFeed(new DotPropertyMapping());

        $dataRequest = [
            'data_type' => 'url',
            'data' => 'https://laravel-frontend.fazwaz.com/properties-20items.json'
        ];

        $result = $xmlFeed->mapping($dataRequest);

        $this->assertArrayHasKey('data', $result);
        $this->assertArrayHasKey('status_code', $result);

        $this->assertEquals(200, $result['status_code']);

        $this->assertArrayHasKey(1, $result['data']['properties']['property']);
        $item = $result['data']['properties']['property'][1];

        $this->assertNotEmpty($item);

        //property ID
        $this->assertArrayHasKey('property_id', $item);
        $this->assertNotEmpty($item['property_id']);

        // Prices []
        $this->assertArrayHasKey('prices', $item);
        $this->assertNotEmpty($item['prices']['price'][0]);

        // prices[price]  @attributes
        $this->assertArrayHasKey('@attributes', $item['prices']['price'][0]);
        $this->assertNotEmpty($item['prices']['price'][0]['@attributes']['tenure']);
        $this->assertNotEmpty($item['prices']['price'][0]['@attributes']['currency']);

        // prices[price]  @value
        $this->assertArrayHasKey('@value', $item['prices']['price'][0]);
        $this->assertNotEmpty($item['prices']['price'][0]['@value']);

        // address[]
        $this->assertArrayHasKey('address', $item);

        // address[province]
        $this->assertArrayHasKey('province', $item['address']);
        $this->assertNotEmpty($item['address']['province']);

        // address[city]
        $this->assertArrayHasKey('city', $item['address']);
        $this->assertNotEmpty($item['address']['city']);

        // address[area]
        $this->assertArrayHasKey('area', $item['address']);
        $this->assertNotEmpty($item['address']['area']);

        // details []
        $this->assertArrayHasKey('details', $item);

        // details[type]
        $this->assertArrayHasKey('type', $item['details']);
        $this->assertNotEmpty($item['details']['type']);

        // details [descriptions]
        $this->assertArrayHasKey('descriptions', $item['details']);
        $this->assertNotEmpty($item['details']['descriptions']);

        // details [descriptions][description]
        $this->assertArrayHasKey('description', $item['details']['descriptions']);
        $this->assertNotEmpty($item['details']['descriptions']['description']);

        // details [descriptions][description]  @attributes
        $this->assertArrayHasKey('@attributes', $item['details']['descriptions']['description'][0]);
        $this->assertNotEmpty($item['details']['descriptions']['description'][0]['@attributes']['lang']);

        // details [descriptions][description] @value
        $this->assertArrayHasKey('@value', $item['details']['descriptions']['description'][0]);
        $this->assertNotEmpty($item['details']['descriptions']['description'][0]['@value']);

        // details [images]
        $this->assertArrayHasKey('images', $item);
        $this->assertNotEmpty($item['images']);

        // images []
        $this->assertArrayHasKey('image', $item['images']);
        $this->assertNotEmpty($item['images']['image']);

        // images[image]  @attributes
        $this->assertArrayHasKey('@attributes', $item['images']['image'][0]);
        $this->assertNotEmpty($item['images']['image'][0]['@attributes']['number']);

        // images[image]   @value
        $this->assertArrayHasKey('@value', $item['images']['image'][0]);
        $this->assertNotEmpty($item['images']['image'][0]['@value']);

    }

}